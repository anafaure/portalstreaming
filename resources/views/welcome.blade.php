<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Radio</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-image: url("/images/fondo-web.png");
                background-position: center;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .top-left {
                position: absolute;
                left: 10px;
                top: 10px;
            } 

            .top-right2 {
                position: absolute;
                right: 35px;
                top: 200px;
            }

            .caja-scroll{
                height: 350px;
                width:250px;
                overflow-y: scroll;
            }

            .padre {
                background-color: #fafafa;
                margin: 1rem;
                padding: 4rem;
                border: 2px solid #ccc;
                /* IMPORTANTE */
                text-align: center;
            }

            .content {
                text-align: center;
            }
            
            .columna {
                text-align: center;
            }

            .title {
                font-size: 60px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
        
        
            @if (Route::has('login'))
                <div class="top-left links">
                
                    <a href="{{ url('/') }}"><img src="/images/logo.png" heigth="100px" width="100px"> </a>      
                    
                </div>
                <div class="top-right links">
                @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}" style="color:#fff;">Iniciar sesion</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" style="color:#fff;">Registrarme</a>
                        @endif
                    @endauth
                <div style = "bottom: 0;display: flex;left: 0;position: fixed;right: 0;width: 30%;z-index: 1500;overflow: hidden;" >
                    <iframe name="contenedorPlayer" class="cuadroBordeado" allow="autoplay" width="70%" height="110px" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no  src="https://cp.usastreams.com/html5-player-barra-responsive.aspx?stream=http://130.185.144.199:19197/live&fondo=1&formato=mp3&color=5&titulo=2&autoStart=1&vol=5&nombre=TVS+RADIO+88.7">
                        <a href="https://www.usastreams.com/" alt = "Usastreams.com posicionamiento web, Servicios SEO y SEM, servicios streaming" title="crear canal de tv on line ,hosting para radio con autodj ,streaming radio y tv ,Radio en Internet,">streaming hosting ,streaming hosting</a>
                    </iframe>
                </div>
                   
                </div>
            @endif
            
            <div class="content">
                <div class="title m-b-md">
                    Bienvenido a "88.7"
                </div>

                <div class="links">
                
                <iframe width="700" height="400" src="https://www.youtube.com/embed/tgIqecROs5M" frameborder="0" allowfullscreen></iframe>
                </div>

            </div>
             
        </div>
        <div class="top-right2">
                <div class="col-md-8">
                    <div class="caja-scroll">
                     
                        <div class="padre">
                        <a href="#">
                            Ejemplo 1
                        </a>
                        </div>

                        <div class="padre">
                        <span style="color:black;">
                            Ejemplo 2
                        </span>
                        </div>
                        <div class="padre">
                        <span style="color:black;">
                            Ejemplo 3
                        </span>
                        </div>
                        <div class="padre">
                        <span style="color:black;">
                            Ejemplo 4
                        </span>
                        </div>
                    </div>
                </div>
            </div>    
        <footer class="row">
                @include('includes.footer')
        </footer>
    </body>
</html>
