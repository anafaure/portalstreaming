@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Menu</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>Gestion</th>
                                <th>Accion</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>VIDEOS YOUTUBE</td>
                                <td><a href="#"><img src="https://img.icons8.com/material-rounded/24/000000/plus.png"></a><a href="#"><img src="https://img.icons8.com/android/24/000000/edit.png"></a></td>
                                
                            </tr>
                            <tr>
                                <td>NOTICIAS</td>
                                <td><a href="#"><img src="https://img.icons8.com/material-rounded/24/000000/plus.png"></a><a href="#"><img src="https://img.icons8.com/android/24/000000/edit.png"></a></td>
                                
                                
                            </tr>
                            <tr>
                                <td>PATROCINADORES</td>
                                <td><a href="#"><img src="https://img.icons8.com/material-rounded/24/000000/plus.png"></a><a href="#"><img src="https://img.icons8.com/android/24/000000/edit.png"></a></td>
                                
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection
